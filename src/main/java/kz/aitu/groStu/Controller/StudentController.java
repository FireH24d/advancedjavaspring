package kz.aitu.groStu.Controller;

import kz.aitu.groStu.Repository.StudentRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Sort;
@RestController
public class StudentController {
    private final StudentRepository studentRepository;


    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }
    @GetMapping("/student/order/{groupId}")
    public ResponseEntity<?> getStudentsById(@PathVariable long groupId) {
        return ResponseEntity.ok(studentRepository.findAllByGroupId(groupId));
    }

    @GetMapping("/student/order")
    public ResponseEntity<?> getStudents() {
        return ResponseEntity.ok(studentRepository.orderAllByGroupId());
    }
}

