package kz.aitu.groStu.Repository;

import kz.aitu.groStu.Entity.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {
    @Query(value = "select * from student order by group_id", nativeQuery = true)
    List<Student> orderAllByGroupId();
    @Query(value = "select * from student where group_id = ?", nativeQuery = true)
    List<Student> findAllByGroupId(long groupId);
}
