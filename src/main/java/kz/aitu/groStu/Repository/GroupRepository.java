package kz.aitu.groStu.Repository;

import kz.aitu.groStu.Entity.Group;
import kz.aitu.groStu.Entity.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends CrudRepository<Group, Long> {

}
